library(tidyverse)
cv <- function(x)sd(x) / mean(x) * 100
files<-list.files("raw_data/",pattern = "expression",recursive = T)%>%
  str_c("raw_data/",.)
xyz<-NULL
for(i in rev(files)){
  df<-i%>%
    readRDS()
  sp<-i%>%
    str_remove("raw_data/")%>%
    str_remove("/.*")
  xyz<-tibble(sp,nrow(df))%>%
    bind_rows(xyz)
}

i<-files[7]
for(i in rev(files)){
  df<-i%>%
    readRDS()
  sp<-i%>%
    str_remove("raw_data/")%>%
    str_remove("/.*")
  
  df%>%
    colnames()%>%
    enframe()%>%
    filter(value != "target_id")%>%
    write_tsv(str_c(sp,"experiments.tsv"))
  if(!"tbl_df" %in% class(df)) df<-as_tibble(df,rownames ="target_id")
  
  
  df_long<-df%>%
    pivot_longer(values_to = "tpm",names_to = "experiment",-target_id)
  
  
  df_long%>%
    filter(tpm==max(tpm))%>%
    write_tsv(str_c(sp,"_hightest_expressed_gene.tsv"))
  
  df_stats<-df_long%>%
    group_by(target_id)%>%
    summarize(min=min(tpm),
              max=max(tpm),
              sd=sd(tpm),
              cov=cv(tpm),
             median=median(tpm),
              mean=mean(tpm))
  df_stats%>%
    write_tsv(str_c(sp,"_stats_genes.tsv"))
}

networks<-list.files("raw_data/",pattern = "network.Rds",recursive = T)%>%
  str_c("raw_data/",.)

stats

for(i in networks){
  sp<-i%>%
    str_remove_all("raw_data/|/network.Rds")
  
  network<-i%>%
    readRDS()
  network%>%
    colnames()%>%
    enframe()%>%
    filter(value!="target_id")%>%
    write_tsv(str_c(sp,"_tfs.tsv"))
}



files<-list.files(pattern="stats_genes.tsv")[c(4,5,6,8,10,11,12,13)]
tfs<-list.files(pattern="_tfs.tsv")

all_species_stats<-NULL
for(i in files){
  species<-str_remove(i,"_stats_genes.tsv")
  tfs<-list.files(pattern=str_c(species,"_tfs.tsv"))%>%
    read_tsv()
  all_species_stats<-i%>%
    read_tsv()%>%
    mutate(species=species)%>%
    mutate(species=str_c(str_sub(species,1,1),". ",str_sub(species,2)))%>%
    mutate(tf=if_else(target_id%in%tfs$value,"TF","gene"))%>%
    bind_rows(all_species_stats)
}



all_species_stats%>%
  filter(tf!="gene")%>%
  group_by(species)%>%
  summarise(x=sum(max>1)/n())
  

all_species_stats%>%
  group_by(species)%>%
  filter(tf!="gene")%>%
  summarise(median=median(median))
  group_by(max)

stats<-all_species_stats%>%
  group_by(species)%>%
  reframe(quantile=quantile(cov,probs=0.25/2,na.rm=T))


abcdf<-list.files("processed_data/",pattern=".tsv",recursive = T)%>%
  enframe()%>%
  filter(str_detect(value,"gene_lists_fig_1"))%>%
  filter(str_detect(value,"CBBc|photores|PS"))%>%
  mutate(value=str_c("processed_data/",value))
abcdf_r<-NULL
for(i in abcdf$value){
  abcdf_r<-i%>%
    read_tsv()%>%
    bind_rows(abcdf_r)
}


all_species_stats%>%
  mutate(xx=cov<50)%>%
  mutate(PS=target_id%in%abcdf_r$target_id)%>%
  summarise(n_tf=sum(xx,na.rm=T)/n(),n_PS=sum(PS,na.rm=T)/n())

all_species_stats%>%
  mutate(housekeeping=cov<50)%>%
  mutate(PS=target_id%in%abcdf_r$target_id)%>%
  group_by(housekeeping,PS)%>%
  summarise(n=n())
  summarise(n_tf=sum(housekeeping,na.rm=T)/n(),n_PS=sum(housekeeping,na.rm=T)/n())


all_species_stats%>%
  left_join(stats)%>%
  group_by(species)%>%
  summarise(n=n(),freq=sum(cov<50,na.rm = T)/n())


all_species_stats%>%
  group_by(species)%>%
  filter(mean==max(mean))

all_species_stats%>%
  ggplot(aes(x=species,y=cov,fill=tf))+
  geom_boxplot()


x<-list.files(pattern="experiments.tsv")
all_n_exp<-NULL
for(i in x){
  species<-i%>%
    str_remove("experiments.tsv")
  n_experiments<-i%>%
    read_tsv()%>%
    nrow()
  all_n_exp<- tibble(species,n_experiments)%>%
    bind_rows(all_n_exp)
}


x<-list.files(pattern="_tfs.tsv")
n_tfs<-NULL
for(i in x){
  species<-i%>%
    str_remove("_tfs.tsv")
  tfs<-i%>%
    read_tsv()%>%
    nrow()
  n_tfs<- tibble(species,tfs)%>%
    bind_rows(n_tfs)
}


orth_regs<-read_tsv("results/orthologue_comparisons/comparison_of_orthologue_regulators.tsv")

orth_regs%>%
  filter(!str_detect(species,"Crein|old"))%>%
  group_by(species)%>%
  mutate(conserved=if_else(p_value<1e-10,T,F))%>%
  summarise(conservatoin_freq=sum(conserved)/n())%>%
  arrange(conservatoin_freq)

orth_regs%>%
  filter(str_detect(species,"Crein"))%>%
  filter(!str_detect(species,"AtTFs"))%>%
  distinct(orthogroup)

  
