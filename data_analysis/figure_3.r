library(tidyverse)
library(RColorBrewer)
library(ggupset)
library(cowplot)

plot_x_order<-c("C. reinhardtii"=1,"A. thaliana"=2, "T. aestivum"=3, "H. vulgare"=4, "B. distachyon"=5, "O. sativa"=6,"S. bicolor"=7, "Z. mays"=8)%>%
  enframe("species","plot_order")
color_vector_species<-c("Z. mays"="#7FC97F","T. aestivum"="#BEAED4","S. bicolor"="#FDC086","O. sativa"="#FFFF99","H. vulgare"="#386CB0","C. reinhardtii"="#F0027F","B. distachyon"="#BF5B17","A. thaliana"="#666666")


#read data and add species order and color vector
df<-read_excel("data/Supplement_table.xlsx", sheet="Table S7",skip=2 )%>%
  mutate(species=str_c(str_sub(species,1,1),". ",str_sub(species,2,-1)))%>%
  left_join(plot_x_order)%>%
  arrange(plot_order)

genes_pathway<-read_excel("data/Supplement_table.xlsx", sheet="Table S3",skip=1 )

#separate by each species
x<-df%>%
  group_by(species)%>%
  group_split()


#add all upset plots into a plot list
all_upset_plots<-list()
z=1
for(i in x){
  z=i$plot_order[1]
  p<-i%>%
    group_by(TF,species)%>%
    summarize(pathways=list(pathway))%>%
    ungroup()%>%
    ggplot(aes(x = pathways,fill=species)) +
      geom_bar() +
      scale_x_upset()+
      theme_bw()+
      xlab(i$species[1])+
      theme(axis.title.x = element_text(face="italic"))+
      scale_fill_manual(values=color_vector_species)+
      theme(legend.position = "none")
  all_upset_plots[[z]]<-p
  z<-z+1
}
#combine plots and save
p_upset_plot<-plot_grid(plotlist=all_upset_plots,nrow = 2)
ggsave("figure_3_A-H.svg",width =10, height=4)



all_photosynthesis_networks<-read_tsv("data/all_photosynthesis_subnetworks.tsv.gz")%>%
  group_by(species)%>%
  group_split()
all_eigen_values<-NULL
i=all_photosynthesis_networks[[1]]
for(i in all_photosynthesis_networks){
  network_matrix<-i%>%
    select(-species)%>%
    pivot_wider(names_from = "regulator", values_from ="weight")%>%
    column_to_rownames("target_id")
  
  eigenvalues<-network_matrix%>%
    as.matrix()%>%
    bonpow()
  names(eigenvalues)<-c(rownames(network_matrix),colnames(network_matrix))
  all_eigen_values<-enframe(eigenvalues,"gene_id","eigenvalue")%>%
    mutate(species=i$species[1])%>%
    bind_rows(all_eigen_values)
}
#combine eigenvalues with pathway information (remove TFs)
all_eigen_values<-all_eigen_values%>%
  inner_join(genes_pathway)%>%
  left_join(plot_x_order,relationship = "many-to-many")

#plot the median of each pathway and species as a bar plot
pho_eigen_plot<-all_eigen_values%>%
  group_by(species,pathway,plot_order)%>%
  summarise(eigenvalue=median(eigenvalue))%>%
  mutate(pathway=factor(pathway, levels=c("LHC & PS","CBBc","PR","C4")))%>%
  ggplot(aes(x=reorder(species,plot_order),y=eigenvalue,fill=species))+
  geom_bar(stat="identity",color="black")+
  scale_fill_manual(values = color_vector_species)+
  scale_x_discrete(guide = guide_axis(angle = 90))+
  theme_bw()+
  theme(legend.position = "none",
        axis.title.x=element_blank(),
        axis.text.x = element_text(face = "italic"))+
  facet_wrap(~pathway,nrow=1)
ggsave("figure_3_I.svg",width = 10,height=4)

