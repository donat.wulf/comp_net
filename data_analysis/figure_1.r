library(tidyverse)
library(readxl)
library(RColorBrewer)


#get colors
plot_x_order<-c("C. reinhardtii"=1,"A. thaliana"=2, "T. aestivum"=3, "H. vulgare"=4, "B. distachyon"=5, "O. sativa"=6,"S. bicolor"=7, "Z. mays"=8)%>%
  enframe("species","plot_order")
color_vector_species<-c("Z. mays"="#7FC97F","T. aestivum"="#BEAED4","S. bicolor"="#FDC086","O. sativa"="#FFFF99","H. vulgare"="#386CB0","C. reinhardtii"="#F0027F","B. distachyon"="#BF5B17","A. thaliana"="#666666")


#read_in_data
df<-read_excel("data/Supplement_table.xlsx", sheet="Table S6",skip=2)
GOtermresults<-read_tsv("data/all_GO-terms.tsv.gz")

#generate color vector
#extract species names
a<-df%>%
  filter(str_detect(`species comparison`,"Zmays"))%>%
  mutate(species_x=str_extract(`species comparison`,"Zmays"))%>%
  mutate(species_y=str_remove(`species comparison`,"Zmays-|-Zmays"))%>%
  mutate(species_x=str_c(str_sub(species_x,1,1),". ",(str_sub(species_x,2,str_length(species_x)))),
         species_y=str_c(str_sub(species_y,1,1),". ",(str_sub(species_y,2,str_length(species_y)))))





#define plotting order
plot_x_order<-c("C. reinhardtii"=1,"A. thaliana"=2, "T. aestivum"=3, "H. vulgare"=4, "B. distachyon"=5, "O. sativa"=6,"S. bicolor"=7, "Z. mays"=8)%>%
  enframe("species","plot_order")

#generate data frame with number of df with enrichment
plot_data<-GOtermresults%>%
  distinct(transcription_factor,.keep_all = T)%>%
  group_by(species)%>%
  summarize(n=n(),n_sig=sum(classicFisher<1e-10,na.rm=T),frequency=sum(classicFisher<1e-10,na.rm=T)/n())%>%
  mutate(species=str_c(str_sub(species,1,1),". ",(str_sub(species,2,str_length(species)))))%>%
  left_join(plot_x_order,by="species")



p1<-plot_data%>%
  mutate(stat="Number TFs with\n GO term enrichment")%>%
  ggplot(aes(x=reorder(species,plot_order),y=n_sig))+
  geom_bar(stat="identity",aes(fill=species),color="black")+
  scale_fill_manual(values=color_vector_species)+
  scale_x_discrete(guide = guide_axis(angle = 90))+
  theme_bw()+
  theme( plot.caption=element_text(face="italic"),
         legend.position = "none",
         strip.text.x.top=element_text(face="bold"),
         axis.title.x=element_blank(),
         axis.text.x=element_text(face="italic"))+
  ylab("n")+
  facet_wrap(~stat)


p2<-plot_data%>%
  mutate(stat="Frequency of TFs with\n GO term enrichment")%>%
  ggplot(aes(x=reorder(species,plot_order),y=frequency))+
  geom_bar(stat="identity",aes(fill=species),color="black")+
  scale_fill_manual(values=color_vector_species)+
  scale_x_discrete(guide = guide_axis(angle = 90))+
  theme_bw()+
  theme( plot.caption=element_text(face="italic"),
         legend.position = "none",
         strip.text.x.top=element_text(face="bold"),
         axis.title.x=element_blank(),
         axis.text.x=element_text(face="italic"))+
  facet_wrap(~stat)+
  ylim(0,1)

#number of unique to enriched GO terms
plot_3_data_n_goterms_with_top<-GOtermresults%>%
  ungroup()%>%
  filter(complete.cases(.))%>%
  filter(classicFisher<1e-10)%>%
  group_by(transcription_factor)%>%
  slice_min(order_by=classicFisher,n=1)%>%
  arrange(Term)%>%
  distinct(transcription_factor,.keep_all=T)%>%
  group_by(species)%>%
  distinct(GO.ID,.keep_all=T)%>%
  summarise(n=n())%>%
  mutate(species=str_c(str_sub(species,1,1),". ",(str_sub(species,2,str_length(species)))))%>%
  left_join(plot_x_order,by="species")

p3<-plot_3_data_n_goterms_with_top%>%
  mutate(stat="unique number of top\n enriched GOterms")%>%
  ggplot(aes(x=reorder(species,plot_order),y=n))+
    geom_bar(stat="identity",aes(fill=species),color="black")+
    scale_fill_manual(values=color_vector_species)+
    scale_x_discrete(guide = guide_axis(angle = 90))+
    theme_bw()+
    theme( plot.caption=element_text(face="italic"),
           legend.position = "none",
           strip.text.x.top=element_text(face="bold"),
           axis.text.x=element_text(face="italic"),
           axis.title.x=element_blank())+
  facet_wrap(~stat)+
  ylim(0,600)

#get number of GO terms with an enrichment for each species
plot_4_data_goterms_with_sig_enrichment<-GOtermresults%>%
  ungroup()%>%
  filter(complete.cases(.))%>%
  group_by(transcription_factor)%>%
  filter(classicFisher<1e-10)%>%
  arrange(Term)%>%
  group_by(species)%>%
  distinct(GO.ID,.keep_all=T)%>%
  summarise(n=n())%>%
  mutate(species=str_c(str_sub(species,1,1),". ",(str_sub(species,2,str_length(species)))))%>%
  left_join(plot_x_order,by="species")

p4<-plot_4_data_goterms_with_sig_enrichment%>%
  mutate(stat="unique number of\n enriched GOterms")%>%
  ggplot(aes(x=reorder(species,plot_order),y=n))+
  geom_bar(stat="identity",aes(fill=species),color="black")+
  scale_fill_manual(values=color_vector_species)+
  scale_x_discrete(guide = guide_axis(angle = 90))+
  theme_bw()+
  theme( plot.caption=element_text(face="italic"),
         legend.position = "none",
         strip.text.x.top=element_text(face="bold"),
         axis.text.x=element_text(face="italic"),
         axis.title.x=element_blank())+
  facet_wrap(~stat)+
  ylim(0,600)


library(cowplot)
plots<-plot_grid(p1,p2,p4,p3,ncol=2)
ggsave("figure_1_GOterm_stats.svg",plot=plots,width=5, height=6)
