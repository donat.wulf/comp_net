import subprocess
import os
import time
import glob


def make_blast_db(input_fasta,db_type, database_output):
  make_db="makeblastdb -in {in_fasta} -dbtype {dbtype} -out {db_out}"
  make_db_call=make_db.format(in_fasta=input_fasta,db_out=database_output,dbtype=db_type)
  subprocess.run(make_db_call.split(" "))
  return "database done: "+make_db_call

def blast(blast_type, query_file,database_file,output_file,evalue_cutoff,n_threads,max_target_seqs):
  blast="blast{blasttype} -query {query} -db {database} -out {output} -evalue {evalue} -outfmt 6 -num_threads {multi} -max_target_seqs {max_targets}"
  blast_call=blast.format(blasttype=blast_type,query=query_file,database=database_file,output=output_file,evalue=evalue_cutoff,multi=n_threads, max_targets=max_target_seqs)
  subprocess.run(blast_call.split(" "))
  return "blast done: "+blast_call

def run_orthofinder(input_dir,output,n_thread,orthofinder_location="python3 orthofinder"):
  orthofinder=orthofinder_location+" -t {n_threads} -a {n_threads} -o {output_dir} -f {input_dir}"
  orthofinder_call=orthofinder.format(input_dir=input_dir,output_dir=output,n_threads=n_thread)
  subprocess.run(orthofinder_call.split(" "))
  return "orthofinder done: "+orthofinder_call


##### Mapping Skript #######


# Downloads and unpacks SRA files form NCBI
def unpack_sra_file(Run_ID,fasterqdump="fasterq-dump",ncores=8,remove=True):
  #run fasterq-dump
  call=fasterqdump+" -e "+str(ncores)+" --split-files {SRAFile}"
  call=call.format(SRAFile=Run_ID).split(" ")
  try:
    subprocess.run(call)#, stderr=os.DEVNULL, stdout=os.DEVNULL
  except:
    print("Error_while_downloading "+Run_ID+" ".join(call))
  #get output files and return them
  output=sorted(glob.glob(Run_ID+"*.fastq"))
  #check if the file is single or paired end
  if len(output) ==1:
    seq_type="SINGLE"
  else:
    seq_type="PAIRED"
  #add files and Paired or Single into a list
  output_file=[" ".join(output),seq_type]
  
  return output_file

def kallisto_mapping(index,files,type_,output,kallisto="kallisto",ncores=8,remove=True):
  print(files)
  for i in files.split(" "):
    if not os.path.isfile(i):
      for j in files.split(" "):
        if os.path.isfile(j):
          os.remove(j)
      return "file_not_found"
  if type_=="SINGLE":
    call=kallisto+" quant -t "+str(ncores)+" -i {index} -o {output}_out --single -l 200 -s 20 {fastqFiles}"
  else:
    call=kallisto+" quant -t "+str(ncores)+" -i {index} -o {output}_out {fastqFiles}"
  call=call.format(index=index,output=output,fastqFiles=files).split(" ")
  try:
    subprocess.run(call)
  except:
    print("Error while mapping "+output+" ".join(call))
  for i in files.split(" "):
    os.remove(i)
  return "done"


# downloads and maps multiple Runs from SRA
def download_and_map_multiple(index,link_p,output,fasterqdump="fasterq-dump",kallisto="kallisto",ncores=8,remove=True):
  all_fastq=""
  all_types=[]
  if type(link_p)==type("a"):
    link_p=[link_p]
  for i in link_p:
    new_files=unpack_sra_file(i,fasterqdump,ncores,remove)
    all_types.append(new_files[1]) # add all types to a list
    all_fastq+=new_files[0] # add all unpacked files to a string separated by " "
    all_fastq+=" "
  if len(set(all_types))!=1:  #test if all paired or single runs are the same.
    return "mixed of single-end and paired-end Runs!"
  type_p=all_types[0]
  all_fastq=all_fastq.rstrip(" ")
  print(all_fastq)
  kallisto_mapping(index,all_fastq,type_p,output,kallisto,ncores,remove)
  for i in all_fastq.split(" "):
    if os.path.exists(i):
      os.remove(i)
  return "done"


def generate_kallisto_index(transcriptom,index_out,kallisto="kallisto"):
  call=[kallisto,"index","-i",index_out,transcriptom]
  subprocess.run(call)
  return index_out

#run_orthofinder(input_dir="OrthoFinder/ExampleData/",output="orthologues/test_run",n_thread="1")

#make_blast_db("processed_data/A_thaliana/protein.fa","prot","processed_data/A_thaliana/prot_db")
#blast("p","processed_data/H_vulgare/protein.fa","processed_data/A_thaliana/prot_db","processed_data/H_vulgare/At_blastp.tsv","1e-5","10","5")
