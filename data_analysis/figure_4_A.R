library(tidyverse)
library(cowplot)
library(ggupset)

#define colors and plotting order
plot_x_order<-c("C. reinhardtii"=1,"A. thaliana"=2, "T. aestivum"=3, "H. vulgare"=4, "B. distachyon"=5, "O. sativa"=6,"S. bicolor"=7, "Z. mays"=8)%>%
  enframe("species","plot_order")
color_vector_species<-c("Z. mays"="#7FC97F","T. aestivum"="#BEAED4","S. bicolor"="#FDC086","O. sativa"="#FFFF99","H. vulgare"="#386CB0","C. reinhardtii"="#F0027F","B. distachyon"="#BF5B17","A. thaliana"="#666666")

#read in data
df<-read_excel("data/Supplement_table.xlsx", sheet="Table S7",skip=2 )

conserved_regulators<-read_excel("data/Supplement_table.xlsx", sheet="Table S6",skip=2 )%>%
  mutate(p_value=as.numeric(p_value))

#get all photosynthesis regulators
Photosynthesis_regulators<-df%>%
  filter(str_detect(pathway,"CBBc|photorespiration|PS"))%>%
  mutate(gene_list="photosynthesis_regulators")

#get all C4 regulators
C4_regulators<-df%>%
  mutate(gene_list="C4_regulators")%>%
  mutate(domain="C4")

#combine these photosynthesis and C4 regulators
all_regs<-bind_rows(Photosynthesis_regulators,C4_regulators)


orthogroups<-conserved_regulators%>%
  select(gene_id=gene_id_x,orthogroup)%>%
  bind_rows(select(conserved_regulators,gene_id_x,orthogroup))%>%
  distinct(gene_id,orthogroup)


xyz<-all_regs%>%
  left_join(orthogroups,by=c("TF"="gene_id"))%>%
  arrange(p_value)%>%
  distinct(TF,orthogroup,.keep_all = T)%>%
  dplyr::select(TF,p_value,symbol,full_name,domain,species,orthogroup,best_Athaliana_hit)%>%
  group_by(best_Athaliana_hit)%>%
  distinct(species,best_Athaliana_hit,.keep_all = T)%>%
  filter(!is.na(best_Athaliana_hit))%>%
  mutate(species=str_c(str_sub(species,1,1),". ",(str_sub(species,2,str_length(species)))))%>%
  select(TF,domain,species,best_Athaliana_hit,symbol)%>%
  mutate(symbol=if_else(is.na(symbol),best_Athaliana_hit,symbol))%>%
  group_by(symbol)%>%
  summarise(n=n())%>%
  right_join(xyz)%>%
  arrange(desc(n))%>%
  write_tsv("list_of_conserved_photosynthesis_regulators.tsv")

#group TFs by best blast hit and make an upset plot
p<-all_regs%>%
  filter(species!="Creinhardtii")%>%
  left_join(orthogroups,by=c("TF"="gene_id"))%>%
  arrange(p_value)%>%
  distinct(TF,orthogroup,.keep_all = T)%>%
  dplyr::select(TF,p_value,best_Athaliana_hit,symbol,full_name,domain,species,orthogroup)%>%
  group_by(best_Athaliana_hit)%>%
  distinct(species,best_Athaliana_hit,.keep_all = T)%>%
  filter(!is.na(best_Athaliana_hit))%>%
  mutate(species=str_c(str_sub(species,1,1),". ",(str_sub(species,2,str_length(species)))))%>%
  select(TF,domain,species,best_Athaliana_hit,symbol)%>%
  mutate(symbol=if_else(is.na(symbol),best_Athaliana_hit,symbol))%>%
  ungroup() %>%
  select(species,symbol)%>%
  mutate(is_in=T)%>%
  distinct(species,symbol,is_in)%>%
  group_by(symbol)%>%
  summarize(target_processes=list(species))%>%
  rowwise()%>%
  filter(length(target_processes)>=5)%>%
  ggplot(aes(x = target_processes)) +
    geom_bar() +
    scale_x_upset()+
    theme_bw()+
    theme(axis.title.x = element_text(face="italic"))+
    theme(plot.margin = margin(1,1,1,2, "cm"))


ggsave("conserved_reg_upset_plot.svg",plot=p,width=5,height = 5)
