library(tidyverse)
library(RColorBrewer)
library(cowplot)
#get colors
plot_x_order<-c("C. reinhardtii"=1,"A. thaliana"=2, "T. aestivum"=3, "H. vulgare"=4, "B. distachyon"=5, "O. sativa"=6,"S. bicolor"=7, "Z. mays"=8)%>%
  enframe("species","plot_order")
color_vector_species<-c("Z. mays"="#7FC97F","T. aestivum"="#BEAED4","S. bicolor"="#FDC086","O. sativa"="#FFFF99","H. vulgare"="#386CB0","C. reinhardtii"="#F0027F","B. distachyon"="#BF5B17","A. thaliana"="#666666")

#read in GO term and species conservation file
GOtermresults<-read_tsv("data/all_GO-terms.tsv.gz")

df<-read_excel("data/Supplement_table.xlsx", sheet="Table S6",skip=2 )%>%
  rename(species=`species comparison`)%>%
  mutate(p_value=as.numeric(p_value))%>%
  filter(!is.na(species))


df%>%
  filter(str_detect(species,"Creinhardtii"))%>%
  filter(!str_detect(species,"Creinhardtii-Creinhardtii"))%>%
  mutate(species_x=str_extract(species,"Creinhardtii"))%>%
  mutate(species_y=str_remove(species,"Creinhardtii-|-Creinhardtii"))%>%
  group_by(species_y)%>%
  distinct(orthogroup,.keep_all=T)%>%
  summarise(n=n())


##### Boxplot of p-values
Zmays_conservation<-df%>%
  filter(str_detect(species,"Zmays"))%>%
  mutate(species_x=str_extract(species,"Zmays"))%>%
  mutate(species_y=str_remove(species,"Zmays-|-Zmays"))%>%
  mutate(species_x=str_c(str_sub(species_x,1,1),". ",(str_sub(species_x,2,str_length(species_x)))),
         species_y=str_c(str_sub(species_y,1,1),". ",(str_sub(species_y,2,str_length(species_y)))))%>%
  left_join(plot_x_order,by=c("species_y"="species"))

Athalina_conservation<-df%>%
  filter(str_detect(species,"Athaliana"))%>%
  mutate(species_x=str_extract(species,"Athaliana"))%>%
  mutate(species_y=str_remove(species,"Athaliana-|-Athaliana"))%>%
  mutate(species_x=str_c(str_sub(species_x,1,1),". ",(str_sub(species_x,2,str_length(species_x)))),
         species_y=str_c(str_sub(species_y,1,1),". ",(str_sub(species_y,2,str_length(species_y)))))%>%
  left_join(plot_x_order,by=c("species_y"="species"))

Creinhardtii_conservation<-df%>%
  filter(str_detect(species,"Creinhardtii"))%>%
  mutate(species_x=str_extract(species,"Creinhardtii"))%>%
  mutate(species_y=str_remove(species,"Creinhardtii-|-Creinhardtii"))%>%
  mutate(species_x=str_c(str_sub(species_x,1,1),". ",(str_sub(species_x,2,str_length(species_x)))),
         species_y=str_c(str_sub(species_y,1,1),". ",(str_sub(species_y,2,str_length(species_y)))))%>%
  left_join(plot_x_order,by=c("species_y"="species"))


ortho_conservation_split<-bind_rows(Zmays_conservation,Athalina_conservation,Creinhardtii_conservation)%>%
  filter(species_x!=species_y)%>%
  mutate(p_value=p_value+1e-100)%>%
  arrange(p_value)%>%
  group_by(species)%>%
  distinct(orthogroup,species_x,species_y,.keep_all = T)%>%
  group_by(species_x)%>%
  group_split()


plot_breaks<-c(1e-10,1e-30,1e-50,1e-70,1e-90,1e-100)
plot_breaks_log<--log10(plot_breaks)
plot_list_ortho_conservation<-list()
n=1
for(i in ortho_conservation_split[c(1,3,2)]){
  
  plot_list_ortho_conservation[[n]]<-ggplot(i,aes(reorder(species_y,plot_order),-log10(p_value))) +
    geom_boxplot(aes(fill = species_y),outlier.size = 0.8)+
    theme_bw()+
    scale_y_continuous(breaks = plot_breaks_log)+
    theme(axis.text.x = element_text(angle = 90))+
    geom_hline(yintercept=-log10(1e-10+1e-100), linetype="dashed")+
    scale_fill_manual( values=color_vector_species)+
    facet_wrap(~species_x)+
    scale_x_discrete(guide = guide_axis(angle = 90))+
    theme( plot.caption=element_text(face="italic"),
           axis.text.x=element_blank(),
           legend.position = "none",
           strip.text.x.top=element_text(face="italic"),
           axis.title.x=element_blank(),
           plot.margin = unit(c(1,1,0,1), "lines"))+
    ylab("")+
    ylim(0,100)
  
  n<-n+1
}

ortho_conservation<-plot_grid(plotlist=plot_list_ortho_conservation,nrow=1)


##### frequency plot
sig_ogroup<-df%>%
  group_by(species)%>%
  arrange(p_value)%>%
  distinct(orthogroup,species,.keep_all = T)%>%
  summarize(sig_orthogroups=sum(p_value<1e-10),ns_orthogroups=sum(p_value>=1e-10),n_orthogroups=n(),median_p=median(p_value),quantile_p=quantile(p_value,probs=0.25),min_p=min(p_value))%>%
  mutate(ratio=sig_orthogroups/n_orthogroups)


Zmays_conservation<-sig_ogroup%>%
  filter(str_detect(species,"Zmays"))%>%
  mutate(species_x=str_extract(species,"Zmays"))%>%
  mutate(species_y=str_remove(species,"Zmays-|-Zmays"))%>%
  mutate(species_x=str_c(str_sub(species_x,1,1),". ",(str_sub(species_x,2,str_length(species_x)))),
         species_y=str_c(str_sub(species_y,1,1),". ",(str_sub(species_y,2,str_length(species_y)))))%>%
  left_join(plot_x_order,by=c("species_y"="species"))

Athalina_conservation<-sig_ogroup%>%
  filter(str_detect(species,"Athaliana"))%>%
  mutate(species_x=str_extract(species,"Athaliana"))%>%
  mutate(species_y=str_remove(species,"Athaliana-|-Athaliana"))%>%
  mutate(species_x=str_c(str_sub(species_x,1,1),". ",(str_sub(species_x,2,str_length(species_x)))),
         species_y=str_c(str_sub(species_y,1,1),". ",(str_sub(species_y,2,str_length(species_y)))))%>%
  left_join(plot_x_order,by=c("species_y"="species"))

Creinhardtii_conservation<-sig_ogroup%>%
  filter(str_detect(species,"Creinhardtii"))%>%
  mutate(species_x=str_extract(species,"Creinhardtii"))%>%
  mutate(species_y=str_remove(species,"Creinhardtii-|-Creinhardtii"))%>%
  mutate(species_x=str_c(str_sub(species_x,1,1),". ",(str_sub(species_x,2,str_length(species_x)))),
         species_y=str_c(str_sub(species_y,1,1),". ",(str_sub(species_y,2,str_length(species_y)))))%>%
  left_join(plot_x_order,by=c("species_y"="species"))




conservation_data<-bind_rows(Zmays_conservation,Athalina_conservation,Creinhardtii_conservation)%>%
  filter(species_x!=species_y)%>%
  group_by(species_x)%>%
  group_split()
plot_list_ratio<-list()
n=1
for(i in conservation_data[c(1,3,2)]){
  
  plot_list_ratio[[n]]<-ggplot(i,aes(reorder(species_y,plot_order),ratio)) +
    geom_bar(stat="identity",aes(fill = species_y), colour="black")+
    theme_bw()+
    theme(axis.text.x = element_text(angle = 90))+
    scale_fill_manual( values=color_vector_species)+
    facet_wrap(~species_x)+
    scale_x_discrete(guide = guide_axis(angle = 90))+
    theme( plot.caption=element_text(face="italic"),
           axis.text.x=element_blank(),
           legend.position = "none",
           strip.background = element_blank(),
           strip.text.x = element_blank(),
           axis.title.x=element_blank(),
           plot.margin = unit(c(0,1,0,1), "lines"))+
    ylab("")+
    ylim(0,1)
  
  n<-n+1
}

p_number_of_conserved_tfs_with_same_orthogroup<-plot_grid(plotlist=plot_list_ratio,nrow=1)


plot_breaks<-c(1e-10,1e-30,1e-50,1e-70,1e-90)


all_comparisons_without_innerspecies<-df%>%
  separate(species,into = c("species1","species2"),sep = "-",remove =F)%>%
  filter(species1!=species2)%>%
  distinct(species)

######### number of conserved GOterm plot

conserved_TFs_with_GOterm<-df%>%
  filter(species %in% all_comparisons_without_innerspecies$species)%>%
  filter(p_value<1e-10)%>%
  left_join(GOtermresults,by=c("gene_id_x"="transcription_factor"))%>%
  left_join(GOtermresults,by=c("gene_id_y"="transcription_factor"),suffix=c("_x","_y"))%>%
  filter(GO.ID_x==GO.ID_y)%>%
  distinct(GO.ID_x,gene_id_x,gene_id_y,orthogroup,.keep_all=T)

number_of_conserved_regulators_with_same_GOterm<-conserved_TFs_with_GOterm%>%
  filter(classicFisher_x<1e-10&classicFisher_y<1e-10)%>%
  group_by(species.x,Term_x,GO.ID_x)%>%
  summarise(n=n())

Zmays_conserved_GOterms<-conserved_TFs_with_GOterm%>%
  filter(str_detect(species.x,"Zmays"))%>%
  mutate(comp_species=str_extract(species.x,"Zmays"))%>%
  mutate(comp_species2=str_remove(species.x,"Zmays-|-Zmays"))%>%
  filter(comp_species!=comp_species2)%>%
  group_by(comp_species2)%>%
  distinct(comp_species,GO.ID_x, Term_x)

Athaliana_conserved_GOterms<-conserved_TFs_with_GOterm%>%
  filter(str_detect(species.x,"Athaliana"))%>%
  mutate(comp_species=str_extract(species.x,"Athaliana"))%>%
  mutate(comp_species2=str_remove(species.x,"Athaliana-|-Athaliana"))%>%
  filter(comp_species!=comp_species2)%>%
  group_by(comp_species2)%>%
  distinct(comp_species,GO.ID_x, Term_x)

Creinhardtii_conserved_GOterms<-conserved_TFs_with_GOterm%>%
  filter(str_detect(species.x,"Creinhardtii"))%>%
  mutate(comp_species=str_extract(species.x,"Creinhardtii"))%>%
  mutate(comp_species2=str_remove(species.x,"Creinhardtii-|-Creinhardtii"))%>%
  filter(comp_species!=comp_species2)%>%
  group_by(comp_species2)%>%
  distinct(comp_species,GO.ID_x, Term_x)


################ get stats for text #########################
Athaliana_conserved_GOterms%>%
  group_by(GO.ID_x)%>%
  filter(n() == 7)%>%
  distinct(Term_x)

terms_in_A.thaliana_and_monocots<-Athaliana_conserved_GOterms%>%
  filter(comp_species2!="Creinhardtii")%>%
  group_by(GO.ID_x)%>%
  filter(n() == 6)%>%
  distinct(Term_x)
terms_in_A.thaliana_and_monocots%>%
  write_tsv("GOS_specific_to_monocots_and_AT.tsv")

bind_rows(Zmays_conserved_GOterms,Athaliana_conserved_GOterms,c)%>%
  write_tsv("conserved_GOs_between_species.tsv")

Creinhardtii_conserved_GOterms%>%
  group_by(GO.ID_x)%>%
  filter(n()>4)%>%
  filter( n()!=7)%>%
  distinct(Term_x)%>%
  write_tsv("fuzzliy_conserved_in_Creinhardtii.tsv")




Zmays_conserved_GOterms%>%
  filter(comp_species2!="Creinhardtii" &comp_species2!="Athaliana" )%>%
  group_by(GO.ID_x)%>%
  filter(n() == 5)%>%
  distinct(Term_x)%>%
  filter(!Term_x %in% terms_in_A.thaliana_and_monocots$Term_x)%>%
  write_tsv("GOS_specific_to_monocots.tsv")


monocot_specific<-Zmays_conserved_GOterms%>%
  filter(comp_species2!="Creinhardtii" &comp_species2!="Athaliana" )%>%
  group_by(GO.ID_x)%>%
  filter(n() == 5)%>%
  distinct(Term_x)%>%
  filter(!Term_x %in% terms_in_A.thaliana_and_monocots$Term_x)


GOtermresults%>%
  filter(GO.ID %in% monocot_specific$GO.ID_x & species=="Athaliana")%>%
  distinct(GO.ID,Term)%>%View()

Athaliana_conserved_GOterms%>%
  filter(GO.ID_x %in% monocot_specific$GO.ID_x)%>%
  group_by(Term_x,GO.ID_x)%>%
  summarise(n=n())%>%
  write_tsv("GOS_specific_to_monocots_in_how_many_monocots.tsv")


Creinhardtii_conserved_GOterms%>%
  group_by(comp_species2)%>%
  distinct(Term_x)%>%
  summarise(n=n())

plot_x_order<-plot_x_order%>%
  mutate(species_short=str_remove(species,"\\. "))

plot_data<-bind_rows(Zmays_conserved_GOterms,Athaliana_conserved_GOterms,Creinhardtii_conserved_GOterms)%>%
  group_by(comp_species,comp_species2)%>%
  distinct(Term_x)%>%
  summarise(n=n())%>%
  mutate(comp_species=str_c(str_sub(comp_species,1,1),". ",(str_sub(comp_species,2,str_length(comp_species)))),
         comp_species2=str_c(str_sub(comp_species2,1,1),". ",(str_sub(comp_species2,2,str_length(comp_species2)))))%>%
  left_join(plot_x_order,by=c("comp_species2"="species"))%>%
  group_split()
  
plot_list<-list()
n=1
for(i in plot_data[c(1,3,2)]){
  plot_list[[n]]<-i%>%
    ggplot(aes(x=reorder(comp_species2,plot_order) ,y=n))+
    geom_bar(stat="identity",aes(fill = comp_species2), colour="black")+
    scale_fill_manual( values=color_vector_species)+
    facet_wrap(~comp_species)+
    theme_bw()+
    scale_x_discrete(guide = guide_axis(angle = 90))+
    theme( plot.caption=element_text(face="italic"),
           axis.text.x=element_text(face="italic"),
           legend.position = "none",
           axis.title.x=element_blank(),
           strip.background = element_blank(),
           strip.text.x = element_blank(),
           plot.margin = unit(c(0,1,1,1), "lines"))+
    ylab("")
    
  n=n+1
}

### combine all subplots
p_number_of_conserved_tfs_with_same_GOterm<-plot_grid(plotlist=plot_list,nrow=1)



library(ggpubr)
all_plots<-list()
n=1
for(i in 1:3){
  new_plotlsit<-list(plot_list_ortho_conservation[[i]],plot_list_ratio[[i]],plot_list[[i]])
  all_plots[[i]]<-plot_grid(plotlist=new_plotlsit,ncol=1,nrow=3,align ="v",rel_heights =c(1,0.3,0.7))
}
  

p<-plot_grid(plotlist=all_plots ,nrow=1)


ggsave("figure_2.svg",plot=p,width = 15/1.9,height=9/1.4)







