# Comparative analysis
Contains functions and code to perform comparative network analysis.




Input are network files from GENIE3 with target genes in rows and regulators in columns.






Network cutoff was set to 0.005


Dependencies: tidyverse, doParallel




# Data analysis
Contains code for the figures 1-5 and table 2. Additionally the scripts for the identification of C4 and photosynthesis regulators is available.



# Data
Supplement data and all data required to generate the figures.


Due to file size constraints networks are not available online, but can be shared upon request.

